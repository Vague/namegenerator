#!/bin/bash
# vague
# simple adjective noun name generator

# search for specific length 
maxLen=99
if [ ! -z "$1"  ] ; then
	maxLen=$(($1 - 1))
fi

first=$(cat adjectives.txt | awk '{ if (length($0) <= '"$(($maxLen / 2))"') print($0) }' | shuf -n 1)
second=$(cat nounlist.txt | awk '{ if (length($0) <= '"$(($maxLen / 2))"') print($0) }' | shuf -n 1)
second=$(echo $(echo ${second:0:1} | tr '[a-z]' '[A-Z]')${second:1})
echo $first$second$(( RANDOM % 100)) | cut -c -$(( $maxLen +1 ))

